-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema EPAM_DB_HM1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema EPAM_DB_HM1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `EPAM_DB_HM1` DEFAULT CHARACTER SET utf8 ;
USE `EPAM_DB_HM1` ;

-- -----------------------------------------------------
-- Table `EPAM_DB_HM1`.`Gender`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EPAM_DB_HM1`.`Gender` (
  `gender` ENUM('Male', 'Female') NOT NULL,
  PRIMARY KEY (`gender`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EPAM_DB_HM1`.`Family_Tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EPAM_DB_HM1`.`Family_Tree` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Surname` VARCHAR(45) NULL,
  `Name` VARCHAR(45) NULL,
  `Birth_Date` DATE NULL,
  `Death_Date` DATE NULL DEFAULT NULL,
  `Place_of_birth` VARCHAR(45) NULL,
  `Place_of_death` VARCHAR(45) NULL DEFAULT NULL,
  `Marrige_Date` DATE NULL DEFAULT NULL,
  `CreditCard_Number` VARCHAR(45) NULL,
  `Gender_gender` ENUM('Male', 'Female') NOT NULL,
  `Family_Tree_id` INT NOT NULL,
  `NameConcated` VARCHAR(45) NULL DEFAULT 'CONCAT(Name, Surname)',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `CreditCard_Number_UNIQUE` (`CreditCard_Number` ASC) VISIBLE,
  INDEX `fk_Family_Tree_Gender1_idx` (`Gender_gender` ASC) VISIBLE,
  INDEX `fk_Family_Tree_Family_Tree1_idx` (`Family_Tree_id` ASC) VISIBLE,
  CONSTRAINT `fk_Family_Tree_Gender1`
    FOREIGN KEY (`Gender_gender`)
    REFERENCES `EPAM_DB_HM1`.`Gender` (`gender`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Family_Tree_Family_Tree1`
    FOREIGN KEY (`Family_Tree_id`)
    REFERENCES `EPAM_DB_HM1`.`Family_Tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EPAM_DB_HM1`.`Family_siblings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EPAM_DB_HM1`.`Family_siblings` (
  `id` INT NOT NULL,
  `Surname` VARCHAR(45) NULL,
  `Name` VARCHAR(45) NULL,
  `Birth_Date` DATE NULL,
  `Death_Date` DATE NULL DEFAULT NULL,
  `Place_of_birth` VARCHAR(45) NULL,
  `Place_of_death` VARCHAR(45) NULL DEFAULT NULL,
  `Marrige_Date` DATE NULL DEFAULT NULL,
  `Gender_gender` ENUM('Male', 'Female') NOT NULL,
  `Family_Tree_id` INT NOT NULL,
  `Info` VARCHAR(255) NULL,
  PRIMARY KEY (`Family_Tree_id`, `id`),
  INDEX `fk_Family_siblings_Gender_idx` (`Gender_gender` ASC) VISIBLE,
  INDEX `fk_Family_siblings_Family_Tree1_idx` (`Family_Tree_id` ASC) VISIBLE,
  CONSTRAINT `fk_Family_siblings_Gender`
    FOREIGN KEY (`Gender_gender`)
    REFERENCES `EPAM_DB_HM1`.`Gender` (`gender`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Family_siblings_Family_Tree1`
    FOREIGN KEY (`Family_Tree_id`)
    REFERENCES `EPAM_DB_HM1`.`Family_Tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EPAM_DB_HM1`.`Family_values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EPAM_DB_HM1`.`Family_values` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Oriented_Value` VARCHAR(45) NULL,
  `Max_Value` VARCHAR(45) NULL,
  `Min_Value` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EPAM_DB_HM1`.`Family_Tree_has_Family_values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EPAM_DB_HM1`.`Family_Tree_has_Family_values` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Family_Tree_id` INT NOT NULL,
  `Family_values_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Family_Tree_has_Family_values_Family_values1_idx` (`Family_values_id` ASC) VISIBLE,
  INDEX `fk_Family_Tree_has_Family_values_Family_Tree1_idx` (`Family_Tree_id` ASC) VISIBLE,
  CONSTRAINT `fk_Family_Tree_has_Family_values_Family_Tree1`
    FOREIGN KEY (`Family_Tree_id`)
    REFERENCES `EPAM_DB_HM1`.`Family_Tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Family_Tree_has_Family_values_Family_values1`
    FOREIGN KEY (`Family_values_id`)
    REFERENCES `EPAM_DB_HM1`.`Family_values` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
